require 'docker_spec_helper'

describe 'Container spec' do

  describe package('nginx') do
    it { should be_installed }
  end

  describe package('nodejs') do
    it { should be_installed }
  end
end

