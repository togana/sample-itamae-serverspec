require 'serverspec'
require 'docker'

set :backend, :docker
set :docker_url, "tcp://localhost:4243"
# set :docker_image, "nginx:latest"
# nodejs installed
set :docker_image, "hoge:latest"

Excon.defaults[:ssl_verify_peer] = false

