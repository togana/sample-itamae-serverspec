require 'spec_helper'

describe 'localhost spec' do

  describe package('docker-engine') do
    it { should be_installed }
  end

  describe port(4243) do
    it { should be_listening }
  end

end
