execute "install docker" do
  command "curl -sSL https://get.docker.com/ | sh"
  not_if "docker -v"
end

remote_file "/etc/default/docker" do
  owner "root"
  group "root"
  mode "644"
  notifies :restart, 'service[docker]'
end

service 'docker' do
  action :nothing
end


